# Example with function f(x) = x^2 + 10sin(x)
from __future__ import division, print_function, unicode_literals

import numpy as np


def grad(theta):
    q = np.cos(theta)
    res = 2 * theta + 10 * np.cos(theta)
    return 2 * theta + 10 * np.cos(theta)


# check convergence : hoi tu
def has_converged(theta_new, grad):
    return np.linalg.norm(grad(theta_new)) / len(theta_new) < 1e-3
    # return np.linalg.norm()


def GD_momentum(theta_init, grad, eta, gamma):
    # suppose we want to store history of theta
    theta = [theta_init]
    v_old = np.zeros_like(theta_init)
    for it in range(100):
        v_new = gamma * v_old + eta * grad(theta[-1])
        theta_new = theta[-1] - v_new
        if has_converged(theta_new, grad):
            break
        theta.append(theta_new)
        v_old = v_new
    return theta


theta = GD_momentum(np.array([5]), grad, 0.1, 0.9)
print('theta: ', theta[-1][0])
